

using TaskApi.Cliente.ControllerLibrarys;
using TaskApi.DTos;

namespace TaskApi.Cliente.ResponseHandler
{
    public class CommandLineUser
    {
        private readonly UserLibrary userLibrary;
        public CommandLineUser(UserLibrary userLibrary)
        {
            this.userLibrary = userLibrary;
        }
            
        public async Task ProcessCommand(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Uso: dotnet run <comando> [<argumentos>]");
                return;
            }

            string command = args[0];
            switch (command)
            {
                case "update-user":
                    await UpdateUser(args);
                    break;

                case "delete-user":
                    await DeleteUser(args);
                    break;
                case "get-user-by-id":
                    await GetUserId(args);
                    break;

            }
        }
        private async Task UpdateUser(string [] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Usage: dotnet run update-user <id> <username> <password>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid id))
            {
                Console.WriteLine("Invalid user ID.");
                return;
            }

            var userToUpdate = new UserLoginDto
            {
                UserName = args[2],
                Password = args[3]
            };

            var updatedUser = await userLibrary.UpdateUser(id, userToUpdate);
            if (updatedUser != null)
            {
                Console.WriteLine("User actulizado correctamente:");
                Console.WriteLine($"ID: {updatedUser.Id}, Username: {updatedUser.Username}");
            }
            else
            {
                Console.WriteLine("Error user no actualizado.");
            }
        }
        private async Task DeleteUser(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Uso: dotnet run delete-user <id>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid userId))
            {
                Console.WriteLine("ID del user no válido.");
                return;
            }

            var deletedUser = await userLibrary.DeleteUser(userId);
            if (deletedUser != null)
            {
                Console.WriteLine("User eliminada con éxito:");
                Console.WriteLine($"ID: {deletedUser.Id}");
            }
            else
            {
                Console.WriteLine("Error al eliminar el user.");
            }
        }
        private async Task GetUserId(string [] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Uso: dotnet run get-user-by-id <id>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid id))
            {
                Console.WriteLine("User id invalido.");
                return;
            }

            var user = await userLibrary.GetUserById(id);
            if (user != null)
            {
                Console.WriteLine("User Encontrado:");
                Console.WriteLine($"ID: {user.Id}, Username: {user.Username}");
            }
            else
            {
                Console.WriteLine("User no encontrado.");
            }

        }
        
    }
}