
using TaskApi.Cliente.ControllerLibrarys;
using TaskApi.DTos;
using TaskApi.Models.EnumsTask;

namespace TaskApi.Cliente.ResponseHandler
{
    public class CommandLineTasks
    {
        private readonly TaskLibrary taskLibrary;
        public CommandLineTasks(TaskLibrary taskLibrary)
        {
            this.taskLibrary = taskLibrary;
        }
            
        public async Task ProcessCommand(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Uso: dotnet run <comando> [<argumentos>]");
                return;
            }

            string command = args[0];
            switch (command)
            {
                case "create-task":
                    await CreateTask(args);
                    break;

                case "update-task":
                    await UpdateTask(args);
                    break;

                case "delete-task":
                    await DeleteTask(args);
                    break;
                case "get-task-user-priority":
                    await GetGroupsByUserId(args);
                    break;
                case "get-history-status-task":
                    await GetStatusTask(args);
                    break;
            }
        }

        private async Task CreateTask(string[] args)
        {
            if (args.Length < 7 || args.Length > 9)
            {
                Console.WriteLine("Uso: dotnet run create-task <title> <description> <creationDate> <completionDate> <status> <priority> [<userId>] [<groupId>]");
                return;
            }

            var creatorTaskDto = new CreatorTaskDto
            {
                taskTitle = args[1],
                taskDescription = args[2],
                taskCreationDate = DateTime.Parse(args[3]),
                taskCompletationDate = DateTime.Parse(args[4]),
                enumStatusTask = (EnumStatusTask)Enum.Parse(typeof(EnumStatusTask), args[5], true),
                enumPriorityTasks = (EnumPriorityTasks)Enum.Parse(typeof(EnumPriorityTasks), args[6], true)
            };

            if (args.Length >= 8 && Guid.TryParse(args[7], out Guid userId))
            {
                creatorTaskDto.userId = userId;
            }
            else
            {
                Console.WriteLine("Advertencia: El ID de usuario no se proporcionó o es inválido. La tarea se creará sin un usuario asociado.");
            }

            if (args.Length >= 9 && Guid.TryParse(args[8], out Guid groupId))
            {
                creatorTaskDto.groupId = groupId;
            }
            else
            {
                Console.WriteLine("Advertencia: El ID de grupo no se proporcionó o es inválido. La tarea se creará sin un grupo asociado.");
            }

            var createdTask = await taskLibrary.CreateTask(creatorTaskDto);

            if (createdTask != null)
            {
                Console.WriteLine("Tarea creada con éxito:");
                Console.WriteLine($"ID: {createdTask.Id}\n" +
                    $"Título: {createdTask.TaskTitle}\n" +
                    $"Descripción: {createdTask.TaskDescription}\n" +
                    $"Fecha de creación: {createdTask.TaskCreationDate}\n" +
                    $"Fecha de finalización: {createdTask.TaskCompletionDate}\n" +
                    $"Estado: {createdTask.EnumStatusTask}\n" +
                    $"Prioridad: {createdTask.EnumPriorityTasks}\n" +
                    $"ID de usuario: {createdTask.UserId}\n" +
                    $"ID de grupo: {createdTask.GroupId}");

            }
            else
            {
                Console.WriteLine("Error al crear la tarea.");
            }
        }
        private async Task UpdateTask(string[] args)
        {
            if (args.Length != 10)
            {
                Console.WriteLine("Uso: dotnet run update-task <id> <title> <description> <creationDate> <completionDate> <status> <priority> <userId> <groupId>");
                return;
            }

            Guid id;
            if (!Guid.TryParse(args[1], out id))
            {
                Console.WriteLine("Error: El ID de la tarea no es válido.");
                return;
            }

            var updateDto = new CreatorTaskDto
            {
                taskTitle = args[2], 
                taskDescription = args[3], 
                taskCreationDate = DateTime.Parse(args[4]), 
                taskCompletationDate = DateTime.Parse(args[5]),
                enumStatusTask = (EnumStatusTask)Enum.Parse(typeof(EnumStatusTask), args[6], true),
                enumPriorityTasks = (EnumPriorityTasks)Enum.Parse(typeof(EnumPriorityTasks), args[7], true),
                userId = Guid.Parse(args[8]),
                groupId = Guid.Parse(args[9])
            };



            var updatedTask = await taskLibrary.UpdateTask(id, updateDto);
            if (updatedTask != null)
            {
                Console.WriteLine("Tarea actualizada con éxito:");
                Console.WriteLine($"ID: {updatedTask.Id}\n" +
                    $"Título: {updatedTask.TaskTitle}\n" +
                    $"Descripción: {updatedTask.TaskDescription}\n" +
                    $"Fecha de creación: {updatedTask.TaskCreationDate}\n" +
                    $"Fecha de finalización: {updatedTask.TaskCompletionDate}\n" +
                    $"Estado: {updatedTask.EnumStatusTask}\n" +
                    $"Prioridad: {updatedTask.EnumPriorityTasks}\n" +
                    $"ID de usuario: {updatedTask.UserId}\n" +
                    $"ID de grupo: {updatedTask.GroupId}");
            }
            else
            {
                Console.WriteLine("Error al actualizar la tarea.");
            }
        }
        private async Task DeleteTask(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Uso: dotnet run delete-task <id>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid taskId))
            {
                Console.WriteLine("ID de tarea no válido.");
                return;
            }

            var deletedTask = await taskLibrary.DeleteTask(taskId);
            if (deletedTask != null)
            {
                Console.WriteLine("Tarea eliminada con éxito:");
                Console.WriteLine($"ID: {deletedTask.Id}");
            }
            else
            {
                Console.WriteLine("Error al eliminar la tarea.");
            }
        }
        private async Task GetGroupsByUserId(string [] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Uso: dotnet run get-task-user-priority <user_id>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid groupId))
            {
                Console.WriteLine("ID user no válido.");
                return;
            }

            var tasks = await taskLibrary.GetTaskUserPriority(groupId);
            if (tasks != null)
            {
                foreach (var task in tasks)
                {
                    Console.WriteLine($"ID: {task.Id}, \n Prioridad: {task.EnumPriorityTasks} \n  Titulo: {task.TaskTitle}, \n Descripcion: {task.TaskDescription}");
                }
            }
            else
            {
                Console.WriteLine("Error al obtener las tareas del user");
            }
        
        }
        private async Task GetStatusTask(string [] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Uso: dotnet run get-history-status-task <task>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid idTask))
            {
                Console.WriteLine("ID tarea no válido.");
                return;
            }

            var tasks = await taskLibrary.GetHistorySttusTask(idTask);
            if (tasks != null)
            {
                foreach (var task in tasks)
                {
                    Console.WriteLine($"ID: {task.TaskId}, \n Estado: {task.NewStatus}");
                }
            }
            else
            {
                Console.WriteLine("Erros al obtener el historial de tareas");
            }
        
        }


    }

}
