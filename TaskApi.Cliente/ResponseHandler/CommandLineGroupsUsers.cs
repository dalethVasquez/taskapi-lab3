
using TaskApi.Cliente.ControllerLibrarys;
using TaskApi.DTos;

namespace TaskApi.Cliente.ResponseHandler
{
    public class CommandLineGroupsUsers
    {
        private readonly GroupsLibrary groupsLibrary;
        public CommandLineGroupsUsers(GroupsLibrary groupsLibrary)
        {
            this.groupsLibrary = groupsLibrary;
            
        }
        public async Task ProcessCommand(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Uso: dotnet run <comando> [<argumentos>]");
                return;
            }

            string command = args[0];
            switch (command)
            {
                case "get-groups":
                    await GetGroups();
                    break;

                case "get-group-by-id":
                    await GetGroupsById(args);
                    break;
                case "create-group":
                    await CreateGroup(args);
                    break;
                case "get-groups-user":
                    await GetGroupsByUserId(args);
                    break;
                case "delete-group":
                    await DeleteGroup(args);
                    break;
                case "update-group":
                    await UpdateGroup(args);
                    break;

                default:
                    Console.WriteLine("");
                    break; ;
                
                
            }
        }
        private async Task GetGroups()
        {
            var groups = await groupsLibrary.GetGroups();
            if (groups != null)
            {
                foreach (var group in groups)
                {
                    Console.WriteLine($"ID: {group.Id}, Name: {group.Name}, User: {group.userId}");
                }
            }
            else
            {
                Console.WriteLine("Error to get groupos.");
            }
        }
        private async Task GetGroupsById(string [] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Uso: dotnet run get-group-by-id <group_id>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid groupId))
            {
                Console.WriteLine("ID de grupo no válido.");
                return;
            }
            try
            {
                var group = await groupsLibrary.GetGroupById(groupId);
                if (group != null)
                {
                    Console.WriteLine("Grupo obtenido con éxito:");
                    Console.WriteLine($"ID: {group.Id}, Nombre: {group.Name}, IdUser: {group.userId}");
                }
                else
                {
                    Console.WriteLine("No se pudo obtener el grupo.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al obtener el grupo: {ex.Message}");
            }
        }
        private async Task CreateGroup(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Uso: dotnet run create-group <iduser> <titlegroup>");
                return;
            }

            
            var title = args[1];
            if (!Guid.TryParse(args[2], out Guid iduser))
            {
                Console.WriteLine("El ID del usuario no es válido.");
                return;
            }


            var response = await groupsLibrary.CreateGroup(title, iduser );
            if (response is Microsoft.AspNetCore.Mvc.OkObjectResult okObjectResult)
            {
                var groupDto = okObjectResult.Value;
                Console.WriteLine("Grupo creado con éxito:");
                Console.WriteLine(groupDto);
            }
            else
            {
                Console.WriteLine("Error al crear el grupo.");
            }
        }
        private async Task GetGroupsByUserId(string [] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Uso: dotnet run get-group-of-user <user_id>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid groupId))
            {
                Console.WriteLine("ID user no válido.");
                return;
            }

            var groups = await groupsLibrary.GetUserGroups(groupId);
            if (groups != null)
            {
                foreach (var group in groups)
                {
                    Console.WriteLine($"ID: {group.Id}, Name: {group.Name}, User: {group.userId}");
                }
            }
            else
            {
                Console.WriteLine("Error to get groupos.");
            }
        
        }
        private async Task DeleteGroup(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Uso: dotnet run delete-group <id>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid groupId))
            {
                Console.WriteLine("ID de grupo no válido.");
                return;
            }

            var deletedGroup = await groupsLibrary.DeleteGroup(groupId);
            if (deletedGroup != null)
            {
                Console.WriteLine("Grupo eliminado con éxito:");
                Console.WriteLine($"ID: {deletedGroup.Id}, Título: {deletedGroup.Name}");
            }
            else
            {
                Console.WriteLine("Error al eliminar el grupo.");
            }
        }
        private async Task UpdateGroup(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Uso: dotnet run update-group <id> <groupName> <idUser>");
                return;
            }

            if (!Guid.TryParse(args[1], out Guid id))
            {
                Console.WriteLine("Id grupo invalido.");
                return;
            }
            if (!Guid.TryParse(args[3], out Guid IdUser))
            {
                Console.WriteLine("Invalid user ID.");
                return;
            }

            var creatorGroupDto = new CreatorGroupDto
            {
                Name = args[2],
                userId = IdUser
                
            };

            var updatedGroup = await groupsLibrary.UpdateGroup(id, creatorGroupDto);
            if (updatedGroup != null)
            {
                Console.WriteLine("Grupo actualizado correctamente:");
                Console.WriteLine($"ID: {updatedGroup.Id}, Nombre: {updatedGroup.Name}");
            }
            else
            {
                Console.WriteLine("Error: Grupo no acrualizado.");
            }
        }

        
        
    }
}