

namespace TaskApi.Cliente.ResponseHandler
{
    public class CommandLineAuthUser
    {
        private readonly AuthApiLibraty _apiCliente;

        public CommandLineAuthUser(AuthApiLibraty apiCliente)
        {
            _apiCliente = apiCliente;
        }

        public async Task ProcessCommand(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Uso: dotnet run <comando> [<argumentos>]");
                return;
            }

            string comando = args[0];
            switch (comando)
            {
                case "register-user":
                    await RegisterUser(args);
                    break;

                case "login":
                    await Login(args);
                    break;

                default:
                    Console.WriteLine("");
                    break;
            }
        }

        private async Task RegisterUser(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Uso: dotnet run register-user <username> <password>");
                return;
            }

            var username = args[1];
            var password = args[2];

            var response = await _apiCliente.Register(username, password);
            if (response is Microsoft.AspNetCore.Mvc.OkObjectResult okObjectResult)
            {
                var userDto = okObjectResult.Value;
                Console.WriteLine("Usuario creado con éxito:");
                Console.WriteLine(userDto); 
            }
            else
            {
                Console.WriteLine("Error al registrar el usuario.");
            }
        }

        private async Task Login(string[] args)
        {
           if (args.Length != 3)
            {
                Console.WriteLine("Uso: dotnet run login <username> <password>");
                return;
            }

            var usernameLogin = args[1];
            var passwordLogin = args[2];

            try
            {
                var token = await _apiCliente.Login(usernameLogin, passwordLogin);
                if (!string.IsNullOrEmpty(token))
                {
                    Console.WriteLine("Inicio de sesión exitoso.");
                    Console.WriteLine($"Token: {token}");
                }
                else
                {
                    Console.WriteLine("Error al iniciar sesión. Credenciales incorrectas.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al iniciar sesión: {ex.Message}");
            }
        }
    }
}