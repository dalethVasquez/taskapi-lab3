﻿using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TaskApi.DTos;

namespace TaskApi.Cliente;

public class AuthApiLibraty
{
   private readonly HttpClient _httpClient;
        private readonly string _baseUriApi;

        public AuthApiLibraty(string baseUriApi)
        {
            _baseUriApi = baseUriApi;
            _httpClient = new HttpClient();
        }

        public async Task<IActionResult> Register(string username, string password)
        {
            var loginData = new
            {
                username,
                password
            };
            var requestBody = new StringContent(System.Text.Json.JsonSerializer.Serialize(loginData), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync($"{_baseUriApi}/api/auth/register", requestBody);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var userDto = System.Text.Json.JsonSerializer.Deserialize<UserListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                Console.WriteLine("Id: " + userDto.Id);
                Console.WriteLine("Username: " + userDto.Username);
                return new OkObjectResult(userDto); 
                
            }
            else
            {
                return new BadRequestResult(); 
            }
        }
        public async Task<string> Login(string username, string password)
        {
            var loginDto = new { UserName = username, Password = password };
            var content = new StringContent(JsonConvert.SerializeObject(loginDto), System.Text.Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync($"{_baseUriApi}/api/auth/login", content);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Failed to authenticate");
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            var responseData = JsonConvert.DeserializeAnonymousType(responseContent, new { token = "" });

            return responseData.token;
        }



}
