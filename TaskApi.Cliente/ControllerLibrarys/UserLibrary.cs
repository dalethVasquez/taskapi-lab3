
using System.Text;
using System.Text.Json;
using TaskApi.DTos;

namespace TaskApi.Cliente.ControllerLibrarys
{
    public class UserLibrary
    {
        private readonly string _baseUriApi;
        private readonly HttpClient _httpClient;

        public UserLibrary(string baseUriApi)
        {
            _baseUriApi = baseUriApi;
            _httpClient = new HttpClient();
        }

        public async Task<UserListDto> UpdateUser(Guid id, UserLoginDto userToUpdate)
        {
            var requestBody = new StringContent(JsonSerializer.Serialize(userToUpdate), Encoding.UTF8, "application/json");
            var response = await _httpClient.PutAsync($"{_baseUriApi}/api/user/update-user-by-id/{id}", requestBody);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var updatedUserDto = JsonSerializer.Deserialize<UserListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return updatedUserDto;
            }
            else
            {
                return null;
            }
        }
        public async Task<UserListDto> DeleteUser(Guid id)
        {
            var response = await _httpClient.DeleteAsync($"{_baseUriApi}/api/user/delete-user-by-id/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var deletedUser = JsonSerializer.Deserialize<UserListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return deletedUser;
            }

            return null;
        }
        public async Task<UserListDto> GetUserById(Guid id)
        {
            var response = await _httpClient.GetAsync($"{_baseUriApi}/api/user/get-user-by-id/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var userDto = JsonSerializer.Deserialize<UserListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return userDto;
            }
            else
            {
                return null;
            }
        }

    }
}