using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskApi.DTos;

namespace TaskApi.Cliente.ControllerLibrarys
{
    public class GroupsLibrary
    {
        private readonly string _baseUriApi;
        private readonly HttpClient _httpClient;

        public GroupsLibrary(string baseUriApi)
        {
            _baseUriApi = baseUriApi;
            _httpClient = new HttpClient();
        }
       public async Task<IEnumerable<GroupListDto>> GetGroups()
        {
            var response = await _httpClient.GetAsync($"{_baseUriApi}/api/groups/show-groups");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var groupListDto = JsonSerializer.Deserialize<IEnumerable<GroupListDto>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return groupListDto;
            }
            else
            {
                return null;
            }
        }
        public async Task<GroupListDto> GetGroupById(Guid id)
        {
            var response = await _httpClient.GetAsync($"{_baseUriApi}/api/groups/get-group-by-id/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var groupListDto = JsonSerializer.Deserialize<GroupListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return groupListDto;
            }
            else
            {
                return null;
            }
        }
        public async Task<IActionResult> CreateGroup(string name, Guid userid)
        {
            var group = new
            {
                name,
                userid
            };
            var requestBody = new StringContent(System.Text.Json.JsonSerializer.Serialize(group), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync($"{_baseUriApi}/api/groups/create-new-group", requestBody);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var groupDto = System.Text.Json.JsonSerializer.Deserialize<GroupListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                Console.WriteLine("Id: " + groupDto.Id);
                Console.WriteLine("UserId: " + groupDto.userId);
                Console.WriteLine("NameGroup: " + groupDto.Name);
                return new OkObjectResult(group); 
                
            }
            else
            {
                return new BadRequestResult(); 
            }
        }
        public async Task<IEnumerable<GroupListDto>> GetUserGroups(Guid id)
        {
            var response = await _httpClient.GetAsync($"{_baseUriApi}/api/groups/get-group-of-user/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var userGroups = JsonSerializer.Deserialize<IEnumerable<GroupListDto>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return userGroups;
            }
            else
            {
                return null;
            }
        }
        public async Task<GroupListDto> DeleteGroup(Guid id)
        {
            var response = await _httpClient.DeleteAsync($"{_baseUriApi}/api/groups/delete-group-by-id/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var deletedGroup = JsonSerializer.Deserialize<GroupListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return deletedGroup;
            }
            else
            {
                return null;
            }
        }
        public async Task<GroupListDto> UpdateGroup(Guid id, CreatorGroupDto creatorGroupDto)
        {
            var requestBody = new StringContent(JsonSerializer.Serialize(creatorGroupDto), Encoding.UTF8, "application/json");
            var response = await _httpClient.PutAsync($"{_baseUriApi}/api/groups/update-group/{id}", requestBody);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var upsdatedGoup = JsonSerializer.Deserialize<GroupListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return upsdatedGoup;
            }
            else
            {
                return null;
            }

        }
        


        

    }
}