

using System.Text;
using System.Text.Json;
using TaskApi.DTos;
using TaskApi.Models;

namespace TaskApi.Cliente.ControllerLibrarys
{
    public class TaskLibrary
    {
        private readonly string _baseUriApi;
        private readonly HttpClient _httpClient;

        public TaskLibrary(string baseUriApi)
        {
            _baseUriApi = baseUriApi;
            _httpClient = new HttpClient();
        }
         public async Task<TaskListDto> CreateTask(CreatorTaskDto creatorTaskDto)
        {
            var requestBody = new StringContent(JsonSerializer.Serialize(creatorTaskDto), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync($"{_baseUriApi}/api/tasks/create-new-task", requestBody);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var createdTaskDto = JsonSerializer.Deserialize<TaskListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return createdTaskDto;
            }
            else
            {
                return null;
            }
        }
        public async Task<TaskListDto> UpdateTask(Guid id, CreatorTaskDto updateDto)
        {
            var requestBody = new StringContent(JsonSerializer.Serialize(updateDto), Encoding.UTF8, "application/json");
            var response = await _httpClient.PutAsync($"{_baseUriApi}/api/tasks/update-task-existent/{id}", requestBody);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var updatedTaskDto = JsonSerializer.Deserialize<TaskListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return updatedTaskDto;
            }
            else
            {
                return null;
            }
        }
        public async Task<TaskListDto> DeleteTask(Guid id)
        {
            var response = await _httpClient.DeleteAsync($"{_baseUriApi}/api/tasks/delete-by-id-task/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var deletedTask = JsonSerializer.Deserialize<TaskListDto>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return deletedTask;
            }
            else
            {
                return null;
            }
        }
        public async Task<IEnumerable<TaskListDto>> GetTaskUserPriority(Guid id)
        {
            var response = await _httpClient.GetAsync($"{_baseUriApi}/api/tasks/get-task-by-user-priority/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var userTask = JsonSerializer.Deserialize<IEnumerable<TaskListDto>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return userTask;
            }
            else
            {
                return null;
            }
        }
        public async Task<IEnumerable<HistoryTasksEntity>> GetHistorySttusTask(Guid id)
        {
            var response = await _httpClient.GetAsync($"{_baseUriApi}/api/tasks/get-history-of-status-task/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var userTask = JsonSerializer.Deserialize<IEnumerable<HistoryTasksEntity>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                return userTask;
            }
            else
            {
                return null;
            }
        }
        
    }
}