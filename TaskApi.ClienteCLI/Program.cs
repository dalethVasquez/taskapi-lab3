﻿
using TaskApi.Cliente;
using TaskApi.Cliente.ControllerLibrarys;
using TaskApi.Cliente.ResponseHandler;

public class Init{
    static async Task Main(string[] args)
    {
        string connectionApi = "http://localhost:5296";
        /// Auth Userss
        var apiCliente = new AuthApiLibraty(connectionApi);
        var commandProcessor = new CommandLineAuthUser(apiCliente);
        await commandProcessor.ProcessCommand(args);

        //Groups

        var clientGroups  = new GroupsLibrary(connectionApi);
        CommandLineGroupsUsers commandLineGroupsUsers = new CommandLineGroupsUsers(clientGroups);
        await commandLineGroupsUsers.ProcessCommand(args);

        //Tasks
       var clientTasks = new TaskLibrary(connectionApi);
        CommandLineTasks commandLineTasks= new CommandLineTasks(clientTasks);
        await commandLineTasks.ProcessCommand(args);
        //Users
        var clientUser = new UserLibrary(connectionApi);
        CommandLineUser commandLineUser = new CommandLineUser(clientUser);
        await commandLineUser.ProcessCommand(args);




    }
}