
using System.ComponentModel.DataAnnotations;


namespace TaskApi.DTos
{
    public class UserRegisterDto
    {
        [Required(ErrorMessage = "The username is required")] 
        public string? username { get; set; }
        [Required(ErrorMessage = "The password is required")]
        private string?  password { get; set; }
        public string? Password
        {
            get { return password; }
            set
            {
                bool isValid = value?.All(char.IsLetterOrDigit) ?? false;
                if (isValid)
                {
                    password = value;
                }
                else
                {
                    throw new ArgumentException("The password must contain only letters and digits.");
                }
            }
        
        }
    }
}