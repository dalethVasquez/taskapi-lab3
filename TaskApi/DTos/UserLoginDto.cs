
using System.ComponentModel.DataAnnotations;

namespace TaskApi.DTos
{
    public class UserLoginDto
    {
        [Required(ErrorMessage = "The username is required")] 
        public string ?UserName { get; set; }
        [Required(ErrorMessage = "The password is required")] 
        public string ?Password {get; set;}    
    }
}