
using TaskApi.Models;

namespace TaskApi.DTos
{
    public class UserListDto
    {
        public Guid Id { get; set; } 
        public string? Username { get; set; }
        public virtual List<TaskEntity> ?tasksUser { get; set;}
        public virtual List<TaskGroupEntity>? taskGroupEntities{get; set;}
    }
}