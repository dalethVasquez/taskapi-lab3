using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApi.Models.EnumsTask;

namespace TaskApi.DTos
{
    public class TaskListDto
    {
        public Guid Id { get; set; }
        public string ? TaskTitle { get; set; }
        public string? TaskDescription { get; set; }
        public DateTime  TaskCreationDate { get; set; }
        public DateTime  TaskCompletionDate { get; set; }
        public EnumStatusTask EnumStatusTask { get; set; }
        public EnumPriorityTasks EnumPriorityTasks { get; set; }
        public Guid UserId { get; set; }
        public Guid GroupId { get; set; }
    }
}