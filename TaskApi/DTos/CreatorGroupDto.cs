
using System.ComponentModel.DataAnnotations;


namespace TaskApi.DTos
{
    public class CreatorGroupDto
    {
        [Required(ErrorMessage = "The name the group is required")]
        public string? Name { get; set; }
        [Required(ErrorMessage = "The id user own group is required")]
        public Guid userId { get; set; }
        
    }
}