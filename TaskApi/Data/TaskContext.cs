
using Microsoft.EntityFrameworkCore;

namespace TaskApi.Models.Context
{
    public class TaskContext : DbContext
    {
        public TaskContext(DbContextOptions<TaskContext> options) : base(options)
        {

        }
        public DbSet<TaskEntity> Tasks { get; set; } = null!;
        public DbSet<TaskGroupEntity> taskGroups{ get; set; } = null!;
        public DbSet<UserEntity> users{ get; set; } = null!;
        public DbSet<HistoryTasksEntity> historyTasksEntities { get; set; } = null!;
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<TaskEntity>(c =>{
                c.HasKey(t => t.Id);
                c.Property(t => t.taskTitle).IsRequired().HasMaxLength(100);
                c.Property(t => t.taskDescription).IsRequired().HasMaxLength(250);
                c.Property(t => t.taskCreationDate).IsRequired().HasMaxLength(100);
                c.Property(t => t.taskCompletationDate).IsRequired();
                c.Property(t => t.enumStatusTask).IsRequired();
                c.Property(t => t.enumPriorityTasks).IsRequired();

                // Relations

                // Un usuario puede tener varias
                 c.HasOne(t => t.User)
                    .WithMany(u => u.tasksUser)
                    .HasForeignKey(t => t.userId);
                
                //Un grupo de tareas puede tener varias tareas
                    c.HasOne(t => t.taskGroupEntity)
                    .WithMany(tg => tg.taskEntities)
                    .HasForeignKey(t => t.groupId);
            });
            builder.Entity<TaskGroupEntity> (tg =>
            {
                tg.HasKey(t => t.Id);
                tg.Property(t => t.Name).IsRequired().HasMaxLength(100);

                // Relations

                // Muchos grupos de tareas pertenecen a un usuario
                tg.HasOne(g => g.UserEntity)
                    .WithMany(u => u.taskGroupEntities)
                    .HasForeignKey(g=> g.userId);   

            });
            builder.Entity<UserEntity> (us => {
                us.HasKey(u => u.Id);
                us.Property(u => u.Username).IsRequired().HasMaxLength(100);
                //Relations

                //Un usuario tiene varios grupos de tareas
                us.HasMany(u => u.taskGroupEntities)
                    .WithOne(g => g.UserEntity)
                    .HasForeignKey(g => g.userId);

                // Un usuario tine varias tareas
                us.HasMany(u => u.tasksUser)
                    .WithOne(t => t.User)
                    .HasForeignKey(t => t.userId);
            });
        builder.Entity<HistoryTasksEntity>(h=>
        {
            h.HasKey(t => t.Id);
            h.Property(t => t.OldStatus);
            h.Property(t => t.NewStatus);
            h.Property(t => t.Timestamp);

            // Relations
            h.HasOne(h => h.taskEntity)
                 .WithMany(t => t.HistoryTasksEntity)
                 .HasForeignKey(h => h.TaskId);

        });
        }
    }
}