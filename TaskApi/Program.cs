using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using TaskApi.Controllers.Dtos;
using TaskApi.Logic.Services.Auth;
using TaskApi.Logic.Services.Implementations;
using TaskApi.Logic.Services.Interfaces;
using TaskApi.Mapper;
using TaskApi.Models.Context;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
/*builder.Services.AddDbContext<TaskContext>(opt =>{
    var config = builder.Configuration.GetConnectionString("DefaultConnection");
    var serverVersion = new MySqlServerVersion(ServerVersion.AutoDetect(config));
    opt.UseMySql(config, serverVersion);
});*/
builder.Services.AddDbContext<TaskContext>(opt =>
    opt.UseSqlite(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddScoped<ServiceTasks>();
builder.Services.AddScoped<ServiceUser>();
builder.Services.AddScoped<ServicesGroupTask>();
builder.Services.AddScoped<IAuthService, AuthUser>();
builder.Services.AddScoped<ITokenServices, TokenService>();
builder.Services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);
builder.Services.AddScoped<ServiceCreatorData>();
builder.Services.AddScoped<AuthUser>();


// token
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                            .GetBytes(builder.Configuration["Token"])),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                    };
                });

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
