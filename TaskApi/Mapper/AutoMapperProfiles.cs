

using AutoMapper;
using TaskApi.DTos;
using TaskApi.Models;

namespace TaskApi.Mapper
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<UserRegisterDto, UserEntity>();
            CreateMap<UserLoginDto, UserEntity>();
            CreateMap<UserEntity, UserListDto>();
            CreateMap<TaskEntity, TaskListDto>();
            CreateMap<CreatorTaskDto, TaskEntity>();
            CreateMap<TaskListDto, TaskEntity>();   
            CreateMap<TaskGroupEntity, GroupListDto>();
            CreateMap<CreatorGroupDto, TaskGroupEntity>();
            CreateMap<GroupListDto, TaskGroupEntity>(); 
            CreateMap<TaskGroupEntity, CreatorGroupDto>();
            CreateMap<TaskEntity, CreatorTaskDto>().ForMember(dest => dest.userId, opt => opt.MapFrom(src => src.Id));
            
        }
        
    }
}