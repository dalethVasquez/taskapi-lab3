
using System.Data;
using Microsoft.EntityFrameworkCore;
using TaskApi.Logic.Factory;
using TaskApi.Logic.Services;
using TaskApi.Logic.Services.Validations;
using TaskApi.Logic.Validations;
using TaskApi.Models;
using TaskApi.Models.Context;

namespace TaskApi.Controllers.Dtos
{
    public class ServiceTasks : IServices<TaskEntity>
    {
        private readonly TaskContext _taskDataContext;
        public ServiceTasks(TaskContext _taskDataContext)
        {
            this._taskDataContext = _taskDataContext;
        } 
        public virtual async Task<IEnumerable<TaskEntity>> GetAll()
        {
            return await _taskDataContext.Tasks.ToListAsync();
        }

        public async Task<TaskEntity> GetById(Guid id)
        {
            return await _taskDataContext.Tasks.FindAsync(id);
        }

        public virtual async Task<TaskEntity> Create(TaskEntity task)
        {
            CreationDateTask.ValidateDate(task);
            if(!IndexEnumCalculator.GetEnumLength(task))
            {
                throw new ArgumentException("Review the index of status or prioriy.");

            }
            task.Id = Guid.NewGuid();
             CreatorStatusTaskHistory.UpdateTaskStatus(task.Id, task.enumStatusTask, task.userId, _taskDataContext);
            _taskDataContext.Tasks.Add(task);
            await _taskDataContext.SaveChangesAsync();
            return task;
        }

        public async Task<bool> Update(Guid id, TaskEntity task)
        {
            if(!ValidatorID.IsValidGuid(id))
            {
                throw new ArgumentException("The id not is valid.");
            }
            
            CreationDateTask.ValidateDate(task);
            if(!IndexEnumCalculator.GetEnumLength(task))
            {
                throw new ArgumentException("Review the index of status or prioriy.");

            }
            if(id != task.Id)
            {
                return false;
            }
            CreatorStatusTaskHistory.UpdateTaskStatus(id, task.enumStatusTask, task.userId, _taskDataContext);
            _taskDataContext.Entry(task).State = EntityState.Modified;
            await _taskDataContext.SaveChangesAsync();
            return true;
        }

        public async Task<TaskEntity> Delete(Guid id)
        {
            if(!ValidatorID.IsValidGuid(id))
            {
                throw new ArgumentException("The id not is valid.");
            }

            var taskToDelete = await _taskDataContext.Tasks.FindAsync(id);
            if (taskToDelete == null)
            {
                throw new ArgumentException("The id not exixt.");;
            }

            _taskDataContext.Tasks.Remove(taskToDelete);
            await _taskDataContext.SaveChangesAsync();
            return taskToDelete;
        }
        public async Task<IEnumerable<TaskEntity>> GetTasksOrderedByPriority(Guid userId)
        {
            if(!ValidatorID.IsValidGuid(userId))
            {
                throw new ArgumentException("The id not is valid.");
            }

            var tasks = await _taskDataContext.Tasks
                .Where(t => t.userId == userId)
                .OrderBy(t => t.enumPriorityTasks)
                .ToListAsync();
            return tasks;
        }
       public async Task<IEnumerable<TaskEntity>> GetTaskByGroup(Guid groupId)
       {
            if(!ValidatorID.IsValidGuid(groupId))
            {
                throw new ArgumentException("The id not is valid.");
            }
            var taskByGroup = await _taskDataContext.Tasks.Where(x => x.groupId == groupId).ToListAsync();
            return taskByGroup;
       }
        public async Task<IEnumerable<HistoryTasksEntity>> GetTaskStatusHistory(Guid taskId)
        {
            var statusHistory = await _taskDataContext.historyTasksEntities
                .Where(history => history.TaskId == taskId)
                .OrderBy(history => history.Timestamp)
                .ToListAsync();
            return statusHistory;
        }
    }
        
}