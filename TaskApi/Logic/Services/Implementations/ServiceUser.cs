
using Microsoft.EntityFrameworkCore;
using TaskApi.Logic.Validations;
using TaskApi.Models;
using TaskApi.Models.Context;

namespace TaskApi.Logic.Services.Implementations
{
    public class ServiceUser : IServices<UserEntity>
    {
        public TaskContext _taskDataContext;
        public ServiceUser(TaskContext taskContext)
        {
            _taskDataContext = taskContext;
        }
        public async Task<UserEntity> Create(UserEntity user)
        {
           throw new ArgumentException();
        }

        public async Task<UserEntity> Delete(Guid id)
        {
            if(!ValidatorID.IsValidGuid(id))
            {
                throw new ArgumentException("The id not is valid.");
            }
            
            var userToDelete = await _taskDataContext.users.FindAsync(id);
            if (userToDelete == null)
            {
                throw new ArgumentException("The id not exixt.");
            }

            _taskDataContext.users.Remove(userToDelete);
            await _taskDataContext.SaveChangesAsync();
            return userToDelete;
        }

        public async Task<UserEntity> GetById(Guid id)
        {
            if(!ValidatorID.IsValidGuid(id))
            {
                throw new ArgumentException("The id not is valid.");
            }

            return await _taskDataContext.users.FindAsync(id);
        }

        public async Task<IEnumerable<UserEntity>> GetAll()
        {
            return await _taskDataContext.users.ToListAsync();
        }

        public async Task<bool> Update(Guid id, UserEntity task)
        {
            if(!ValidatorID.IsValidGuid(id))
            {
                throw new ArgumentException("The id not is valid.");
            }
            
            if(id != task.Id)
            {
                return false;
            }

            _taskDataContext.Entry(task).State = EntityState.Modified;
            await _taskDataContext.SaveChangesAsync();
            return true;
        }
         public async Task<bool> UserExist(string username)
        {
            if (await _taskDataContext.users.AnyAsync(x=>x.Username == username))
            {
                return true;
            }
            return false;
        }
        

    }
}