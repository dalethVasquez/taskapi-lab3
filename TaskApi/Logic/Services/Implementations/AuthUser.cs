

using Microsoft.EntityFrameworkCore;
using TaskApi.Logic.Factory;
using TaskApi.Logic.Services.Interfaces;
using TaskApi.Logic.Validations;
using TaskApi.Models;
using TaskApi.Models.Context;

namespace TaskApi.Logic.Services.Auth
{
    public class AuthUser : IAuthService
    {
        private readonly TaskContext taskContext;

        public AuthUser(TaskContext taskContext)
        {
            this.taskContext = taskContext;
        }

        public async Task<UserEntity?> Login(string username, string password)
        {
            var user = await taskContext.users.FirstOrDefaultAsync(x => x.Username == username);
            if (user == null)
            {
                return null;
            }
            if(!VerifyPassword.VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }
            return user;
            
        }

        public async Task<UserEntity> Register(UserEntity userEntity, string Password)
        {
            byte [] passwordHash;
            byte [] passwordSalt;
            CreatorPasswordHash.CreatePasswordHash(Password, out passwordHash, out passwordSalt);
            userEntity.PasswordHash = passwordHash;
            userEntity.PasswordSalt = passwordSalt;
            await taskContext.users.AddAsync(userEntity);
            await taskContext.SaveChangesAsync();
            return userEntity;

        }

        public async Task<bool> UserExist(string username)
        {
            if (await taskContext.users.AnyAsync(x=>x.Username == username))
            {
                return true;
            }
            return false;
        }
    }
}