using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TaskApi.DTos;
using TaskApi.Logic.Factory;
using TaskApi.Logic.Services.Auth;
using TaskApi.Models;
using TaskApi.Models.Context;
using TaskApi.Models.EnumsTask;

namespace TaskApi.Logic.Services.Implementations
{
    public class ServiceCreatorData
    {
        private readonly TaskContext taskContext;
        private readonly IMapper mapper;
        protected readonly AuthUser authUser;

        public ServiceCreatorData(TaskContext taskContext, IMapper mapper, AuthUser authUser)
        {
            this.taskContext = taskContext;
            this.mapper = mapper;
            this.authUser = authUser;
            
        }
        public async Task CreateAsync(int countUser, int countTasks)
        {
            using (var transaction = await taskContext.Database.BeginTransactionAsync())
            {
                try
                {
                    var existingGroups = await taskContext.taskGroups.ToListAsync();

                    for (int i = 0; i < countUser; i++)
                    {
                        var registerUserDto = new UserRegisterDto
                        {
                            username = $"UserDal{i}",
                            Password = $"UserDal{i}"
                        };

                        var newUser = mapper.Map<UserEntity>(registerUserDto);
                        byte [] passwordHash;
                        byte [] passwordSalt;
                        CreatorPasswordHash.CreatePasswordHash(registerUserDto.Password, out passwordHash, out passwordSalt);
                        newUser.PasswordHash = passwordHash;
                        newUser.PasswordSalt = passwordSalt;
                        await taskContext.users.AddAsync(newUser);

                         if (await authUser.UserExist(registerUserDto.username))
                            {
                                    
                                await authUser.Register(newUser, registerUserDto.Password);
                            }
                        taskContext.users.Add(newUser);

                        for (int j = 0; j < countTasks; j++)
                        {
                            var creatorTaskDto = new CreatorTaskDto
                            {
                                taskTitle = $"Task {j + 1} for UserDal {i + 1}",
                                taskDescription = $"Description for Task {j + 1} of UserDal {i + 1}",
                                taskCreationDate = DateTime.UtcNow,
                                taskCompletationDate = DateTime.UtcNow.AddDays(10),
                                enumStatusTask = GetRandomStatus(),
                                enumPriorityTasks = GetRandomPriority(),
                                userId = newUser.Id,
                            };

                            if (existingGroups.Any())
                            {
                                var randomGroup = existingGroups.ElementAt(new Random().Next(existingGroups.Count()));
                                creatorTaskDto.groupId = randomGroup.Id;
                            }

                            var newTaskEntity = mapper.Map<TaskEntity>(creatorTaskDto);
                            taskContext.Tasks.Add(newTaskEntity);
                        }
                    }

                    await taskContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    throw ex;
                }
            }
        }

        public EnumStatusTask GetRandomStatus()
        {
            var values = Enum.GetValues(typeof(EnumStatusTask));
            var random = new Random();
            return (EnumStatusTask)values.GetValue(random.Next(values.Length));
        }

        public EnumPriorityTasks GetRandomPriority()
        {
            var values = Enum.GetValues(typeof(EnumPriorityTasks));
            var random = new Random();
            return (EnumPriorityTasks)values.GetValue(random.Next(values.Length));
        }


    }
}