
using Microsoft.EntityFrameworkCore;
using TaskApi.Logic.Validations;
using TaskApi.Models;
using TaskApi.Models.Context;

namespace TaskApi.Logic.Services.Implementations
{
    public class ServicesGroupTask : IServices<TaskGroupEntity>
    {
        private readonly TaskContext _taskDataContext;

        public ServicesGroupTask(TaskContext _taskDataContext)
        {
            this._taskDataContext = _taskDataContext;
        } 
        public async Task<TaskGroupEntity> Create(TaskGroupEntity groupEntity)
        {
            groupEntity.Id = Guid.NewGuid();
            _taskDataContext.taskGroups.Add(groupEntity);
            await _taskDataContext.SaveChangesAsync();
            return groupEntity;
        }

        public async Task<TaskGroupEntity> Delete(Guid id)
        {
            if(!ValidatorID.IsValidGuid(id))
            {
                throw new ArgumentException("The id not is valid.");
            }
           
            var groupToDelete = await _taskDataContext.taskGroups.FindAsync(id);
            if (groupToDelete == null)
            {
                throw new ArgumentException("The id not exis");
            }
            _taskDataContext.taskGroups.Remove(groupToDelete);
            await _taskDataContext.SaveChangesAsync();
            return groupToDelete;

        }

        public async Task<IEnumerable<TaskGroupEntity>> GetAll()
        {
            return await _taskDataContext.taskGroups.ToListAsync();
        }

        public async Task<TaskGroupEntity> GetById(Guid id)
        {
            if(!ValidatorID.IsValidGuid(id))
            {
                throw new ArgumentException("The id not is valid.");
            }
            return await _taskDataContext.taskGroups.FindAsync(id);
        }

        public async Task<bool> Update(Guid id, TaskGroupEntity groupTask)
        {
            if(!ValidatorID.IsValidGuid(id))
            {
                throw new ArgumentException("The id not is valid.");
            }

            if(id != groupTask.Id)
            {
                return false;
            }
            _taskDataContext.Entry(groupTask).State = EntityState.Modified;
            await _taskDataContext.SaveChangesAsync();
            return true;

        }
        public async Task<IEnumerable<TaskGroupEntity>> GetGroupsUser(Guid userId)
        {
            if(!ValidatorID.IsValidGuid(userId))
            {
                throw new ArgumentException("The id not is valid.");
            }

            var groupsUser = await _taskDataContext.taskGroups.Where(x => x.userId == userId).ToListAsync();
            return groupsUser;
        }
    }
}