
using TaskApi.Models;

namespace TaskApi.Logic.Services.Interfaces
{
    public interface IAuthService
    {
       Task<UserEntity> Register(UserEntity userEntity, string Password);
       Task<UserEntity> Login(string username, string password);
       Task<bool> UserExist(string username);
    }
}