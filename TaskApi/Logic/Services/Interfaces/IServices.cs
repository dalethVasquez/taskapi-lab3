

using TaskApi.Models;

namespace TaskApi.Logic.Services
{
    public interface IServices<TEntity>
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> GetById(Guid id);
        Task<TEntity> Create(TEntity task);
        Task<bool> Update(Guid id, TEntity task);
        Task<TEntity> Delete(Guid id);
    }
}