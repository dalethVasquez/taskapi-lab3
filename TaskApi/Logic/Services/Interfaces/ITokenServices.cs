using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskApi.Models;

namespace TaskApi.Logic.Services.Interfaces
{
    public interface ITokenServices
    {
        string CreateToken(UserEntity userEntity);
    }
}