
using Microsoft.EntityFrameworkCore;
using TaskApi.Models;
using TaskApi.Models.Context;
using TaskApi.Models.EnumsTask;

namespace TaskApi.Logic.Factory
{
    public class CreatorStatusTaskHistory
    {
        public static async Task UpdateTaskStatus(Guid taskId, EnumStatusTask newStatus, Guid userId, TaskContext taskContext)
        {
            var oldStatus = await taskContext.Tasks
                .Where(task => task.Id == taskId)
                .Select(task => task.enumStatusTask)
                .FirstOrDefaultAsync();

            taskContext.historyTasksEntities.Add(new HistoryTasksEntity
            {
                TaskId = taskId,
                OldStatus = oldStatus,
                NewStatus = newStatus,
                Timestamp = DateTime.Now,
                UserId = userId
            });

            var taskToUpdate = await taskContext.Tasks.FindAsync(taskId);
            taskToUpdate.enumStatusTask = newStatus;
            await taskContext.SaveChangesAsync();
        }
        
    }
}