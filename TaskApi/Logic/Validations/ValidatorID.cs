
namespace TaskApi.Logic.Validations
{
    public class ValidatorID
    {
        public static bool IsValidGuid(Guid id)
        {
            return id != Guid.Empty;
        }
    }
}