
using TaskApi.DTos;
using TaskApi.Models;
using TaskApi.Models.EnumsTask;

namespace TaskApi.Logic.Factory
{
    public class IndexEnumCalculator
    {
        public static bool GetEnumLength(TaskEntity taskEntity ) 
        {
            if (!Enum.IsDefined(typeof(EnumPriorityTasks), taskEntity.enumPriorityTasks))
            {
                return false;
            }

            if (!Enum.IsDefined(typeof(EnumStatusTask), taskEntity.enumStatusTask))
            {
                return false;
            }
            return true;
        }
    }
}