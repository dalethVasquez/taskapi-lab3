
using TaskApi.DTos;
using TaskApi.Models;

namespace TaskApi.Logic.Services.Validations
{
    public class CreationDateTask
    {
        public static void ValidateDate(TaskEntity task){
            var currentDate = DateTime.Now;

            if (task.taskCreationDate.Date != currentDate.Date)
            {
                throw new ArgumentException("The creation date must be equal to the current date.");
            }

            if (task.taskCompletationDate <= task.taskCreationDate)
            {
                throw new ArgumentException("The completion date must be greater than the creation date.");
            }
        }
    }
}