package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import java.sql.*;



public class Main {

    public static void main(String[] args) {
        try {
            Connection mysqlConn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3300/daleth.vasquez.mysql",
                    "root",
                    "development"
            );

            Connection sqliteConn = DriverManager.getConnection("jdbc:sqlite:daleth.vasquez.sqlite.db");

            String[] createTableQueries = {
                    "CREATE TABLE IF NOT EXISTS users (Id TEXT PRIMARY KEY, Username TEXT NOT NULL, PasswordHash BLOB, PasswordSalt BLOB);",
                    "CREATE TABLE IF NOT EXISTS Tasks (Id TEXT PRIMARY KEY, taskTitle TEXT NOT NULL, taskDescription TEXT NOT NULL, taskCreationDate TEXT NOT NULL, taskCompletationDate TEXT NOT NULL, enumStatusTask INTEGER NOT NULL, enumPriorityTasks INTEGER NOT NULL, userId TEXT NOT NULL, groupId TEXT NOT NULL);",
                    "CREATE TABLE IF NOT EXISTS taskGroups (Id TEXT PRIMARY KEY, Name TEXT NOT NULL, userId TEXT NOT NULL);",
                    "CREATE TABLE IF NOT EXISTS historyTasksEntities (Id TEXT PRIMARY KEY, TaskId TEXT NOT NULL, OldStatus INTEGER NOT NULL, NewStatus INTEGER NOT NULL, Timestamp TEXT, UserId TEXT NOT NULL);",
                    "CREATE TABLE IF NOT EXISTS __EFMigrationsHistory (MigrationId TEXT PRIMARY KEY, ProductVersion TEXT NOT NULL);"
            };

            for (String query : createTableQueries) {
                Statement sqliteStmt = sqliteConn.createStatement();
                sqliteStmt.executeUpdate(query);
            }

            migrateTableData(mysqlConn, sqliteConn, "users");
            migrateTableData(mysqlConn, sqliteConn, "Tasks");
            migrateTableData(mysqlConn, sqliteConn, "taskGroups");
            migrateTableData(mysqlConn, sqliteConn, "historyTasksEntities");
            migrateTableData(mysqlConn, sqliteConn, "__EFMigrationsHistory");

            mysqlConn.close();
            sqliteConn.close();

            System.out.println("Migración completada.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void migrateTableData(Connection sourceConn, Connection targetConn, String tableName) throws SQLException {
        Statement sourceStmt = sourceConn.createStatement();
        ResultSet resultSet = sourceStmt.executeQuery("SELECT * FROM " + tableName);

        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        StringBuilder insertQuery = new StringBuilder("INSERT INTO " + tableName + " VALUES (");
        for (int i = 1; i <= columnCount; i++) {
            insertQuery.append("?");
            if (i < columnCount) {
                insertQuery.append(",");
            }
        }
        insertQuery.append(")");
        PreparedStatement targetStmt = targetConn.prepareStatement(insertQuery.toString());

        while (resultSet.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.println("Migrating data for table: " + tableName);
                targetStmt.setObject(i, resultSet.getObject(i));
            }
            targetStmt.executeUpdate();
        }
        resultSet.close();
        sourceStmt.close();
        targetStmt.close();
    }

}