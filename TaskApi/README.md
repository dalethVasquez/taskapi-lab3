## Api Task

### Description
This API is a CRUD of tasks whit a user that is register and have authorization to do CRUD of task and groups of tasks.

#### To create the Api, run the following commands:
```
dotnet new webapi --use-controllers -o TaskApi
cd TaskApi
dotnet add package Microsoft.EntityFrameworkCore.InMemory
code -r ../TaskApi
```
### Within the Api project install the following dependencies:
```
dotnet add package Microsoft.EntityFrameworkCore
```
```
dotnet add package Microsoft.EntityFrameworkCore.Design
```
```
dotnet add package Microsoft.EntityFrameworkCore.SqLite
```
### To migrate our entity to the database, the commands were used:
```
dotnet tool install --global dotnet-ef
```
```
dotnet ef migrations add InitialCreate
```
```
dotnet ef database update
```
- If there are changes in Entitys we will execute the following command:
```
dotnet ef migrations add <name of the update>
```
- After update the databsase with the third comand.
### For install mysql for our datas use the following command:
```
dotnet add package Pomelo.EntityFrameworkCore.MySql
```

### In our database mysql we need make a migration to a sql lite database. First Install sql lite with the next commands:
```
sudo add-apt-repository -y ppa:linuxgndu/sqlitebrowser
```
```
sudo apt-get update
```
```
sudo apt-get install sqlitebrowser
```
### Create a container for the mysql database:
```
version: '3.8'
services:
  db:
    image: mysql:8.0
    container_name: apiTasks-mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: development
      MYSQL_DATABASE: daleth.vasquez.mysql
    ports:
      - '3300:3306'
    volumes:
      - mysqldata:/var/lib/mysql
volumes:
  mysqldata:
```
### In the file appsettings.json put the connection of mysql:
```
  "DefaultConnection": "Server=localhost;Port=3300;Database=daleth.vasquez.mysql;Uid=root;Pwd=development;"
```
### For the use of the database in program.cs put:
```
builder.Services.AddDbContext<TaskContext>(opt =>{
    var config = builder.Configuration.GetConnectionString("DefaultConnection");
    var serverVersion = new MySqlServerVersion(ServerVersion.AutoDetect(config));
    opt.UseMySql(config, serverVersion);
});
```
### We raise the mysql container with the following command
```
sudo docker compose up -d
```

```
TaskApi/TaskApi/containers$ sudo docker compose up -d
```

### For run Api with sqlite in appseting.json put:
```
"DefaultConnection": "Server=MysqMigrationTiSQLite/daleth.vasquez.sqlite.db"
```
### For use the Api with sqlite in program.cs put:
```
builder.Services.AddDbContext<TaskContext>(opt =>
    opt.UseSqlite(builder.Configuration.GetConnectionString("DefaultConnection")));
```



### Configuration the use of JWT
#### To do endopoint privates and publics use security and autorization for this we can use the following commands to configure the basic for our work environment this type of security is by tokens : 
```
dotnet add package Microsoft.IdentityModel.Tokens
```
```
 dotnet add package System.IdentityModel.Tokens.Jwt
```
```
dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer
```
```
dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson
```
### Working with mappers:
#### They allow you to handle application objects in a more natural way, relating them to the database entities. For example in the code there are DTOS. 
```
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection
```
### Import unit test
#### To test the operation of our API, these dependencies are the basics we need to start creating tests:
```
dotnet add package Microsoft.NET.Test.Sdk
```
```
dotnet add package xunit
```
```
dotnet add package xunit.runner.visualstudio
```
```
dotnet add package Moq.AutoMock
```

### Execution and Validations
#### CRUD Task
![alt text](imgtestcases/image2.png)
![alt text](imgtestcases/image.png)
![alt text](imgtestcases/image3.png)
![alt text](imgtestcases/image4.png)
![alt text](imgtestcases/image5.png)
![alt text](imgtestcases/image6.png)
![alt text](imgtestcases/image7.png)

### Enpoints publics
#### Register
- Validations
    - Cannot create a user with a usernmae already in use
    ![alt text](imgtestcases/AuthController/userExist.png)
    - Field emptys
    ![alt text](imgtestcases/AuthController/fieldEmpty.png)

- Good execution

    ![alt text](imgtestcases/AuthController/createUser.png)

#### Login
- Validation
    - If the user not exist in the database not have authorization.

    ![alt text](imgtestcases/AuthController/invalidCredentials.png)

- Good execution
    - If the credentials of user exist in the database the api execute a token and give authorization for the others controllers.
    ![alt text](imgtestcases/AuthController/login.png)

#### Using controllers without authorization
![alt text](imgtestcases/AuthController/whitoutAuthorization.png)

#### Using controller with authorization
- This type of authorization will allow us to use private endpoints. We will need the token that is generated when we log in.
    ![alt text](imgtestcases/AuthController/barerAuthorization.png)

- Put the token in the text box
![alt text](imgtestcases/AuthController/putToken.png)

- When do the previus procedure we will can use the other controllers:
![alt text](imgtestcases/AuthController/createTaskWhitAutorization.png)
- Get task of user by prority
![alt text](imgtestcases/AuthController/taskByPriority.png)
- Create groups of tasks
![alt text](imgtestcases/AuthController/tasksGroups.png)
- Get grups of a User:
![alt text](imgtestcases/AuthController/groupsUser.png)
- You can also do other actions:
![alt text](imgtestcases/completeApi.png)

### Endpoint of status history tasks:
* First we have to update the status of one task:
![alt text](imgtestcases/endpointhistorystatus/updateTask.png)
* Second make the request, and we see like change the status depend of the id of the task:
![alt text](imgtestcases/endpointhistorystatus/historyStatusTask.png)
### Migration
* To create 100 users with 20 task we create a new endpoint, you can se the method in:
```
TaskApi/Logic/Services/Implementations/ServiceCreatorDta
```
* We execute the endpoint:

* The user  and tasks are generate by a bucle.
![alt text](imgtestcases/migration/generateData.png)

* We review the database (thisi one part)
![alt text](imgtestcases/migration/users.png)
* We review the task generates by user:
![alt text](imgtestcases/migration/tasks.png)

Tests:
![alt text](imgtestcases/migration/newuser.png)
![alt text](imgtestcases/migration/loginnewuser.png)
![alt text](imgtestcases/migration/gettaskuser1.png)
![alt text](imgtestcases/migration/gettaskuser1.png)
### ..... (are 20)

### Before the migration we will make a copy of the mysql database in case there are errors in the execution.
### In the terminal of linux ubuntu:
```
sudo docker exec -it apiTasks-mysql bash
```
```
mysqldump -u root -p daleth.vasquez.mysql > /var/lib/mysql/backup.sql
```
```
exit
```
```
 sudo docker cp apiTasks-mysql:/var/lib/mysql/backup.sql ~/Desktop/backup.sql
```
![alt text](image.png)
Steept to the migration from mysql to sqlite:

1. Export all table of the database mysql:
![alt text](imgtestcases/migration/exportMysql.png)
2. Review the tables:
![alt text](imgtestcases/migration/exportTable.png)
3. Ejecute the migrations: 
  * Download the followings extensions:
     ![alt text](imgtestcases/migration/extensionsJava.png)
  * Go to MysqMigrationToSQLite and run the main
  ![alt text](imgtestcases/migration/runMigration.png)
  
### Querys
* Mysql
![alt text](imgtestcases/migration/taskofuser.png)
* SQLite
![alt text](imgtestcases/migration/taskuserlite.png)


* Mysql

![alt text](imgtestcases/migration/counttaskmysql.png)

*SQLite

![alt text](imgtestcases/migration/counttasksqlite.png)

*MySQL

![alt text](imgtestcases/migration/sumastatusMysql.png)

*SQLite

![alt text](imgtestcases/migration/sumastatuslite.png)

## Clien CLI Api

#### We install the library so that we can work with the client:
  * The client library project will contain the logic necessary to interact with the API. This library will encapsulate all the logic for communicating with the API, including building HTTP requests, handling responses, and any other functionality related to communicating with the API.

```
dotnet new classlib -n TaskApi.Cliente
```
![alt text](imgtestcases/cliApi/library.png)

#### We install the CLI client project:
  * Will use the client library to interact with the API. The client project will be responsible for handling the UI logic and user interaction, as well as calling methods provided by the client library as needed to perform operations on the API.
```
dotnet new console -n TaskApi.ClienteCLI
```
![alt text](imgtestcases/cliApi/clientCliApi.png)

### Implementation library:
* We use HttpClient:
  * They contain the logic to make HTTP requests to the different API endpoints and process the responses.
  * And we use the Dtos created in the api for better handling of responses in the cli.
* This library contains a directory "Controller Library" where for each API controller, that is, it is its library for each API controller.

* This library also contains a "Response Handler" where we instantiate each lib and specify how the user should enter data on the command line and also provide the outputs.

![alt text](imgtestcases/cliApi/LibratyApiStrucuture.png)


#### To run de CLI we need to go at projec TasKApi.ClienteCLI
```
 TaskApi/TaskApi.ClienteCLI$ 
```
* This project is the exit for clients, this is where we make client requests with the required commands.

* Then run the commands thata are in the cases of: project TaskApi.Cliente/ResponseHandles in the swich case.

#### AuthUser:
##### Register:

![alt text](imgtestcases/cliApi/registerCli.png)

* Validation

![alt text](imgtestcases/cliApi/validation1Register.png)

![alt text](imgtestcases/cliApi/validation1Register.png)

##### Login
![alt text](imgtestcases/cliApi/login.png)

![alt text](imgtestcases/cliApi/logvalidation1.png)

![alt text](imgtestcases/cliApi/logvalidation2.png)

#### Groups
![alt text](<imgtestcases/cliApi/Screenshot from 2024-05-07 21-18-44.png>)

![alt text](imgtestcases/cliApi/getgroupbyid.png)

![alt text](imgtestcases/cliApi/createGroup.png)

![alt text](imgtestcases/cliApi/groupsUser.png)

![alt text](imgtestcases/cliApi/deleteGroup.png)

![alt text](imgtestcases/cliApi/updateGroup.png)

#### Tasks 
* Create Task whith group:

![alt text](imgtestcases/cliApi/createTask.png)
* Create Task without group:

![alt text](imgtestcases/cliApi/createTaskOption2.png)

* Update Task By id and fields 

  * In this part I think thar is better make other endpoints to update by parts for example alone title, descripcion, status, priority and end date. And may will be best.

  ![alt text](imgtestcases/cliApi/updateTask.png)

* Delete Task created"

![alt text](imgtestcases/cliApi/deleteTask.png)

* Get task of user by priority:
  * Create task to a specific user:
  * We see the validation date:

![alt text](imgtestcases/cliApi/createTaskUser.png)

![alt text](imgtestcases/cliApi/taskUserPriority.png)

* Get histoty of status tasks:
  * In this case we need to update the task
  ![alt text](imgtestcases/cliApi/statusHistoryTask.png)

### Users
* Update user credentials
  * In this cas I think that will be best create endpoint to update alone de username or password.
  * In this case you can't update a user whit a username that exist, but is a username that not exist is posible.

  ![alt text](imgtestcases/cliApi/updateUser.png)

* Get user by id:
  * You need put the user id

![alt text](imgtestcases/cliApi/getUserByID.png)

* Delete User:
 * You need put the user id

 ![alt text](imgtestcases/cliApi/deleteUser.png)

 