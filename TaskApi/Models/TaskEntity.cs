
using System.ComponentModel.DataAnnotations;
using TaskApi.Models.EnumsTask;

namespace TaskApi.Models
{
    public class TaskEntity
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Task title is required")]
        public string? taskTitle { get; set; }
        [Required(ErrorMessage = "Task description is required")]
        public string? taskDescription { get; set; }
        [Required(ErrorMessage = "Task creation date is required")]
        public DateTime taskCreationDate { get; set; }
        [Required(ErrorMessage = "Task completation date is required")]
        public DateTime taskCompletationDate { get; set; }
        [Required(ErrorMessage = "Task status is required")]
        public  EnumStatusTask enumStatusTask{ get; set; }
        [Required(ErrorMessage = "Task priority is required")]
        public EnumPriorityTasks enumPriorityTasks{ get; set; }
        public Guid  userId { get; set; }
        public virtual UserEntity? User { get; set; }
        public Guid groupId{ get; set; }
        public virtual TaskGroupEntity? taskGroupEntity { get; set; }
        public virtual List<HistoryTasksEntity> HistoryTasksEntity{ get; set; }
        public TaskEntity()
        {
            HistoryTasksEntity = new List<HistoryTasksEntity>();
            
        }
    }
    

}
