namespace TaskApi.Models.EnumsTask
{
    public enum EnumPriorityTasks
    {
        High,
        Medium,
        Low,
        Minimum,
        None
    }
}
