
using System.ComponentModel.DataAnnotations;

namespace TaskApi.Models
{
    public class UserEntity
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "The username is required")]
        public string? Username { get; set; }
        public virtual List<TaskEntity> tasksUser { get; set;}
        public virtual List<TaskGroupEntity> taskGroupEntities{get; set;}
        public byte[] ?PasswordHash { get; set; }
        public byte[] ?PasswordSalt { get; set; }

        public UserEntity ()
        {
            tasksUser = new List<TaskEntity> ();
            taskGroupEntities = new List<TaskGroupEntity> ();
        }

        public static implicit operator Guid(UserEntity? v)
        {
            throw new NotImplementedException();
        }
    }
    
}
