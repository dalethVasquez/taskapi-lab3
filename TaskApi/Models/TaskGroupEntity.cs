
using System.ComponentModel.DataAnnotations;


namespace TaskApi.Models
{
    public class TaskGroupEntity
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "The name the group is required")]
        public string? Name { get; set; }
        public Guid userId { get; set; }
        public virtual UserEntity? UserEntity { get; set; }
        public virtual List<TaskEntity> taskEntities { get; set; }
        public TaskGroupEntity()
        {
            taskEntities = new List<TaskEntity>();
        }
    }
}