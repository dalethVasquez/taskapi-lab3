
using System.ComponentModel.DataAnnotations;
using TaskApi.Models.EnumsTask;

namespace TaskApi.Models
{
    public class HistoryTasksEntity
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "The id  Task is required")]
        public Guid TaskId { get; set; } 
        [Required(ErrorMessage = "The status task is required")]
        public EnumStatusTask OldStatus { get; set; }  
        public EnumStatusTask NewStatus { get; set; }        
        public DateTime ? Timestamp { get; set; } 
        [Required(ErrorMessage = "The user id is required")]
        public Guid UserId { get; set; }
        public TaskEntity? taskEntity{ get; set; }
        public UserEntity? User{ get; set; }
        
    }
}