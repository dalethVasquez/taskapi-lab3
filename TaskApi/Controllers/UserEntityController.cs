
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaskApi.DTos;
using TaskApi.Logic.Factory;
using TaskApi.Logic.Services.Implementations;

namespace TaskApi.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/user")]
    public class UserEntityController : ControllerBase
    {
        private readonly ServiceUser serviceUser;
        private IMapper mapper; 
        public UserEntityController(ServiceUser serviceUser, IMapper mapper)
        {
            this.serviceUser = serviceUser;
            this.mapper = mapper;
        } 
        [HttpGet("get-user-by-id/{id}")]
        public async Task<ActionResult> GetUser(Guid id)
        {
            var user = await serviceUser.GetById(id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }
        [HttpPut("update-user-by-id/{id}")]
        public async Task<ActionResult> UpdateUser(Guid id, UserLoginDto userToUpdate)
        {
            try
            {
                var existentUser = await serviceUser.GetById(id);
                if(existentUser == null)
                {
                    return BadRequest("The user not exist.");
                }
                mapper.Map(userToUpdate, existentUser);
                if (await serviceUser.UserExist(userToUpdate.UserName))
                {
                     return BadRequest("The usernme is in use.");
                }

                byte[] newPasswordHash;
                byte[] newPasswordSalt;
                CreatorPasswordHash.CreatePasswordHash(userToUpdate.Password, out newPasswordHash, out newPasswordSalt);
                existentUser.PasswordHash = newPasswordHash;
                existentUser.PasswordSalt = newPasswordSalt;
                await serviceUser.Update(id, existentUser);
                var updatedUserDto = mapper.Map<UserListDto>(existentUser);
                return Ok(updatedUserDto);
            }
            catch(Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");

            }

           
        }
        [HttpDelete("delete-user-by-id/{id}")]
        public async Task<ActionResult> DeleteUser(Guid id)
        {
            try
            {
                var userToDelete = await serviceUser.Delete(id);
                UserListDto userListDto = mapper.Map<UserListDto>(userToDelete);
                return Ok(userListDto);
            }
            catch(ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }
        [HttpDelete("get-all-users")]
         public async Task<ActionResult> GetTasks(){
         var tasks = await serviceUser.GetAll();
            IEnumerable<UserListDto> taskListDtos = mapper.Map<IEnumerable<UserListDto>>(tasks);
            return Ok(taskListDtos);

         }

        
        
    }
}