
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskApi.Controllers.Dtos;
using TaskApi.DTos;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/tasks")]
    public class TaskEntityController : ControllerBase
    {
        private readonly ServiceTasks serviceTasks;
        private readonly IMapper mapper;
        public TaskEntityController(ServiceTasks serviceTasks, IMapper mapper)
        {
            this.serviceTasks = serviceTasks;
            this.mapper = mapper;
        } 
        [HttpGet("show-tasks")]
        public async Task<ActionResult> GetTasks()
        {
            var tasks = await serviceTasks.GetAll();
            IEnumerable<TaskListDto> taskListDtos = mapper.Map<IEnumerable<TaskListDto>>(tasks);
            return Ok(taskListDtos);
        }
 
        [HttpGet("choose-task-by-id/{id}")]
        public async Task<ActionResult> GetTask(Guid id)
        {
            TaskEntity task = await serviceTasks.GetById(id);
            TaskListDto taskListDto = mapper.Map<TaskListDto>(task);

            if (task == null)
            {
                return NotFound();
            }
            return Ok(taskListDto);
        }
 
        [HttpPost("create-new-task")]
        public virtual async Task<ActionResult> CreateTask(CreatorTaskDto creatorTaskDto) 
        {
            try
            {
                var newTaskEntity = mapper.Map<TaskEntity>(creatorTaskDto);
                var createdTask = await serviceTasks.Create(newTaskEntity);
                var createTaskDto = mapper.Map<TaskListDto>(createdTask);
                return CreatedAtAction(nameof(GetTask), new { id = createdTask.Id }, createTaskDto);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut("update-task-existent/{id}")]
        public async Task<ActionResult> Put(Guid id, CreatorTaskDto updateDto)
        {
            try
            {
                var existingTask = await serviceTasks.GetById(id);
                if (existingTask == null)
                {
                    return NotFound("Task not found.");
                }
                mapper.Map(updateDto, existingTask);
                await serviceTasks.Update(id, existingTask);
                var updatedTaskDto = mapper.Map<TaskListDto>(existingTask);

                return Ok(updatedTaskDto);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
       
        [HttpDelete("delete-by-id-task/{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                var deletedTask = await serviceTasks.Delete(id);
                TaskListDto taskListDto = mapper.Map<TaskListDto>(deletedTask);
                return Ok(taskListDto);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        
        }
        [HttpGet("get-task-by-user-priority/{id}")]
        public async Task<ActionResult> GetTaskByPriority(Guid id)
        {
            try
            {
                var tasks = await serviceTasks.GetTasksOrderedByPriority(id);
                IEnumerable<TaskListDto> taskListDto = mapper.Map<IEnumerable<TaskListDto>>(tasks);
                return Ok(taskListDto);


            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);

            }

        }
        [HttpGet("get-task-by-group/{id}")]
        public async Task<ActionResult> GetTaskByGroup(Guid id)
            {
            try
            {
                var tasks = await serviceTasks.GetTaskByGroup(id);
                IEnumerable<TaskListDto> taskListDto = mapper.Map<IEnumerable<TaskListDto>>(tasks);
                return Ok(taskListDto);


            }
            catch (ArgumentException ex){
                return BadRequest(ex.Message);

            }

        }
        [HttpGet("get-history-of-status-task/{id}")]
        public async Task<ActionResult<IEnumerable<HistoryTasksEntity>>> GetTaskStatusHistory(Guid id)
        {
            try
            {
                var statusHistory = await serviceTasks.GetTaskStatusHistory(id);
                return Ok(statusHistory);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        
        
    }
}