
using Microsoft.AspNetCore.Mvc;
using TaskApi.Logic.Factory;
using TaskApi.Logic.Services.Implementations;

namespace TaskApi.Controllers
{
    [ApiController]
    [Route("api/data")]
    public class DataGenerator : ControllerBase
    {
         private readonly ServiceCreatorData _creatorUserWithTasks;
         public DataGenerator(ServiceCreatorData creatorUserWithTasks)
        {
            _creatorUserWithTasks = creatorUserWithTasks;
        }

        [HttpPost("generate")]
        public async Task<IActionResult> GenerateData(int userCount, int taskCount)
        {
            try
            {
                await _creatorUserWithTasks.CreateAsync(userCount, taskCount);
                return Ok("Data generation completed successfully");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error generating data: {ex.Message}");
            }
        }
    }
}