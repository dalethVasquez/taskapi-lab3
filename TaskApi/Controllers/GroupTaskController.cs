
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskApi.DTos;
using TaskApi.Logic.Services.Implementations;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/groups")]
    public class GroupTaskController : ControllerBase
    {
        private readonly ServicesGroupTask servicesGroupTask;
        private readonly IMapper mapper;
        public GroupTaskController(ServicesGroupTask servicesGroupTask, IMapper mapper)
        {
            
            this.servicesGroupTask = servicesGroupTask;
            this.mapper = mapper;
        } 
        [HttpGet("show-groups")]
        public async Task<ActionResult> GetGroups()
        {
            var groups = await servicesGroupTask.GetAll();
            IEnumerable<GroupListDto> groupListDto = mapper.Map<IEnumerable<GroupListDto>>(groups);
            return Ok(groupListDto);
        }
        [HttpGet("get-group-by-id/{id}")]
        public async Task<ActionResult> GetGroupById(Guid id)
        {
            var group = await servicesGroupTask.GetById(id);
            GroupListDto groupListDto = mapper.Map<GroupListDto>(group);
            if (group == null)
            {
                return NotFound("The group id not exist.");
            }
            return Ok(groupListDto);
        }
        [HttpPost("create-new-group")]
        public async Task<ActionResult> CreateGroup(CreatorGroupDto creatorGroupDto) 
        {
            try
            {
                var newEntityGroup = mapper.Map<TaskGroupEntity>(creatorGroupDto);
                var createdGroup = await servicesGroupTask.Create(newEntityGroup);
                var createGroupDto = mapper.Map<GroupListDto>(createdGroup);
                return CreatedAtAction(nameof(GetGroupById), new { id = createdGroup.Id }, createGroupDto);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("get-group-of-user/{id}")]
        public async Task<ActionResult> GetUserGroups(Guid id)
        {
            try
            {
                var groupsUser = await servicesGroupTask.GetGroupsUser(id);
                IEnumerable<GroupListDto> userGroups = mapper.Map<IEnumerable<GroupListDto>>(groupsUser);
                return Ok(userGroups);

            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);

            }

        }
        [HttpDelete("delete-group-by-id/{id}")]
        public async Task<ActionResult> DeleteGroup(Guid id)
        {
            try
            {
                var group = await servicesGroupTask.Delete(id);
                GroupListDto groupListDto = mapper.Map<GroupListDto>(group);
                return Ok(groupListDto);

            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);

            }

        }
        [HttpPut("update-group/{id}")]
        public async Task<ActionResult> UpdateGroup(Guid id, CreatorGroupDto  groupToUpdate)
        {
            try
            {
                var existingGroup = await servicesGroupTask.GetById(id);
                if (existingGroup == null)
                {
                    return NotFound("Group not found.");
                }

                mapper.Map(groupToUpdate, existingGroup);
                await servicesGroupTask.Update(id, existingGroup);
                var updateGroupDto = mapper.Map<GroupListDto>(existingGroup);

                return Ok(updateGroupDto);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }

        }
        
    }
}