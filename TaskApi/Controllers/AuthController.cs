
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaskApi.DTos;
using TaskApi.Logic.Services.Interfaces;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    [ApiController]
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
         private readonly IAuthService _authService;
        private readonly ITokenServices _tokenService;
        private readonly IMapper _mapper;

        public AuthController(IAuthService authService, ITokenServices tokenService, IMapper mapper)
        {
            _authService = authService;
            _tokenService = tokenService;
            _mapper = mapper;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserRegisterDto user)
        {
            if(await _authService.UserExist(user.username))
            {
                return BadRequest("The user exist");
            }
            var newUser = _mapper.Map<UserEntity>(user);
            var createUser = await _authService.Register(newUser, user.Password);
            var createUserDto = _mapper.Map<UserListDto>(createUser);
            return Ok(createUserDto);

        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDto userLoginDto)
        {
            var usuarioFromRepo = await _authService.Login(userLoginDto.UserName, userLoginDto.Password);

            if (usuarioFromRepo == null)
                return Unauthorized();


            var usuario = _mapper.Map<UserListDto>(usuarioFromRepo);

            return Ok(new
            {
                token = _tokenService.CreateToken(usuarioFromRepo),
                usuario
            });
        }
        
    }
}
